const PDFDocument = require('pdfkit');
const QRCode = require('qrcode');
const PDFImage = require('pdf-image').PDFImage;
const tmp = require('tmp');
var fs = require('fs');

// variable
var maxChoice = 120;
const rowOfID = 9;
var subjectName = 'คอมพิวเตอร์';
var examDate = '22 ตุลาคม 2562';
var examTerm = 1;
var examYear = 2562;
var teacherSignature = true;
var suggestion = 'ระบายคำตอบที่ถูกที่สุดให้ชัดเจนด้วยดินสอหรือปากกา กรณีต้องการเปลี่ยนคำตอบ ให้ลบคำตอบเดิมออกให้สะอาด';
var fileName = tmp.tmpNameSync({
    dir: './tmp',
    template: 'MC-XXXXXX.pdf',
});

// Create a document
const doc = new PDFDocument({
    Title: 'Answer Sheet',
    Author: 'Ligament Studio',
    CreationDate: Date(),
    size: 'A4', //[595.28, 841.89]
    margin: 35
});
doc.pipe(fs.createWriteStream(fileName));
doc.font('fonts/Sarabun.ttf')
doc.save()
    .rect(35, 35, 25, 10)
    .rect(35, 796.89, 25, 10)
    .rect(535.28, 796.89, 25, 10).fill('#00')

// เพิ่มโลโก้และวาดกรอบสี่เหลี่ยมด้านล่างโลโก้
doc.image('images/logo.png', 79, 40, {
    fit: [130, 89],
    align: 'center',
    valign: 'center'
});
doc.moveTo(219, 134)
    .lineTo(79, 134)
    .lineTo(79, 204)
    .lineTo(219, 204)
    .lineWidth(.2).stroke()
if (teacherSignature) {
    doc.moveTo(79, 169).lineTo(219, 169).stroke()
    doc.fontSize(14).text('ลงชื่อ...........................................', 79, 142, { width: 140, height: 18, align: 'center' })
        .fontSize(10).text('(ลายมือผู้เข้าสอบ)', 88, 155, { width: 122, height: 14, align: 'center' })
        .fontSize(14).text('ลงชื่อ...........................................', 79, 177, { width: 140, height: 18, align: 'center' })
        .fontSize(10).text('(ลายมือผู้คุมสอบ)', 88, 190, { width: 122, height: 14, align: 'center' })
} else {
    doc.fontSize(14).text('ลงชื่อ...........................................', 79, 155, { width: 140, height: 18, align: 'center' })
        .text('(ลายมือผู้เข้าสอบ)', 88, 172, { width: 122, height: 14, align: 'center' })
}


// วาดกรอบสี่เหลี่ยมครอบข้อมูลส่วนหัวและสร้างข้อมูลส่วนหัว
doc.rect(219, 36, 154, 168).stroke()
doc.fontSize(14).text('ชื่อ..........................................................', 224, 50,
    {
        width: 144,
        align: 'justify'
    })
    .text('นามสกุล.................................................', 224, 75,
        {
            width: 144,
            align: 'justify'
        })
    .text('เลขที่................... ชั้น.............................', 224, 100,
        {
            width: 144,
            align: 'justify'
        })
    .text('วิชา' + subjectName, 224, 125)
    .text('วันที่สอบ ' + examDate, 224, 150)
    .text('ภาคเรียนที่ ' + examTerm.toString(), 224, 175)
    .text('ปีการศึกษา ' + examYear.toString(), 289, 175);


// Studen ID
doc.rect(373, 36, 14 * rowOfID, 14).stroke()
doc.text('รหัสนักเรียน', 373, 34, { width: 14 * rowOfID, align: 'center' });
doc.fontSize(10)
for (let index = 0; index < rowOfID; index++) {
    doc.rect(373 + (index * 14), 50, 14, 14)
        .rect(373 + (index * 14), 64, 14, 140)
        .stroke()
    for (let j = 0; j < 10; j++) {
        doc.circle(380 + (index * 14), 71 + (j * 14), 5).lineWidth(.1).stroke()
            .text(j.toString(), 373 + (index * 14), 65 + (j * 14), { width: 14, height: 14, align: 'center' })
    }
}

// Exam Set
doc.lineWidth(.2).rect(527, 36, 28, 14).stroke()
doc.fontSize(14).text('ชุด', 527, 34, { width: 28, align: 'center' });
doc.fontSize(10)
for (let index = 0; index < 2; index++) {
    doc.rect(527 + (index * 14), 50, 14, 14)
        .rect(527 + (index * 14), 64, 14, 140)
        .stroke()
    for (let j = 0; j < 10; j++) {
        doc.circle(534 + (index * 14), 71 + (j * 14), 5).lineWidth(.1).stroke()
            .text(j.toString(), 527 + (index * 14), 65 + (j * 14), { width: 14, height: 14, align: 'center' })
    }
}

// คำแนะนำ
doc.rect(79, 211, 476, 30).lineWidth(.2).stroke()
    .fontSize(14).text(suggestion, 79, 218, { width: 476, height: 14, align: 'center' })


// Add Choice
const choiceTextThai = ['ก', 'ข', 'ค', 'ง', 'จ']
const choiceTextEng = ['A', 'B', 'C', 'D', 'E']
const choiceTextNum = ['1', '2', '3', '4', '5']
const choiceText = choiceTextThai;
var setOfChoice = 5;
var choiceSpilt = 10;
var choiceNumber = 1;


var gabForRowChoice = 14 * 7;
var numberOfRow = 5;
if (maxChoice % 20 === 0 || maxChoice <= 20) {
    gabForRowChoice = 14 * 9;
    numberOfRow = 4;
}

if (maxChoice > 150) {
    console.log("maxChoice > 150");

}
else if (maxChoice > 100) {
    choiceSpilt = 32;
}
else if (maxChoice > 50) {
    choiceSpilt = 21;
}
else if (maxChoice > 25) {
    choiceSpilt = 10;
}
else {
    choiceSpilt = 5;
}

doc.lineWidth(.1)
for (let r = 0; r < numberOfRow; r++) {
    for (let j = 0; j < setOfChoice; j++) {
        doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' }) // ข้อความบนChoice
    }
    for (let index = 1; index <= choiceSpilt; index++) {
        if (index % 11 === 0) {
            for (let j = 0; j < setOfChoice; j++) {
                doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' }) // ข้อความบนChoice
            }
        }
        else {
            doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' }) // เลขข้อ
            doc.fontSize(10)
            for (let j = 0; j < setOfChoice; j++) {
                doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).stroke()
                    .text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246.7 + (index * 14), { width: 14, height: 14, align: 'center' }) // ข้อความในChoice
            }
        }
    }
}
// if (maxChoice > 50) {

// }
// else if (maxChoice > 25)
// {
//     for (let r = 0; r < numberOfRow; r++) {
//         for (let j = 0; j < setOfChoice; j++) {
//             doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' })
//         }
//         for (let index = 1; index <= 10; index++) {
//             doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             doc.fontSize(10)
//             for (let j = 0; j < setOfChoice; j++) {
//                 doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).lineWidth(.1).stroke()
//                 doc.text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             }
//         }
//     }
// }
// else
// {
//     for (let r = 0; r < numberOfRow; r++) {
//         for (let j = 0; j < setOfChoice; j++) {
//             doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' })
//         }
//         for (let index = 1; index <= 5; index++) {
//             doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             doc.fontSize(10)
//             for (let j = 0; j < setOfChoice; j++) {
//                 doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).lineWidth(.1).stroke()
//                 doc.text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             }
//         }
//     }
// }
// if (maxChoice == 150) {
//     gabForRowChoice = 14 * 7;

// }
// else if (maxChoice == 120) {
//     gabForRowChoice = 14 * 9;
//     for (let r = 0; r < 4; r++) {
//         for (let j = 0; j < setOfChoice; j++) {
//             doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' })
//         }
//         for (let index = 1; index <= 32; index++) {
//             if (index % 11 === 0) {
//                 for (let j = 0; j < setOfChoice; j++) {
//                     doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//                 }
//             }
//             else {
//                 doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//                 doc.fontSize(10)
//                 for (let j = 0; j < setOfChoice; j++) {
//                     doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).lineWidth(.1).stroke()
//                     doc.text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//                 }
//             }
//         }
//     }
// }
// else if (maxChoice == 50) {
//     gabForRowChoice = 14 * 7;
//     for (let r = 0; r < 5; r++) {
//         for (let j = 0; j < setOfChoice; j++) {
//             doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' })
//         }
//         for (let index = 1; index <= 10; index++) {
//             doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             doc.fontSize(10)
//             for (let j = 0; j < setOfChoice; j++) {
//                 doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).lineWidth(.1).stroke()
//                 doc.text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             }
//         }
//     }
// }
// else if (maxChoice == 40) {
//     gabForRowChoice = 14 * 9;
//     for (let r = 0; r < 4; r++) {
//         for (let j = 0; j < setOfChoice; j++) {
//             doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' })
//         }
//         for (let index = 1; index <= 10; index++) {
//             doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             doc.fontSize(10)
//             for (let j = 0; j < setOfChoice; j++) {
//                 doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).lineWidth(.1).stroke()
//                 doc.text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             }

//         }
//     }
// }
// else if (maxChoice == 25) {
//     gabForRowChoice = 14 * 7;
//     for (let r = 0; r < 5; r++) {
//         for (let j = 0; j < setOfChoice; j++) {
//             doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' })
//         }
//         for (let index = 1; index <= 5; index++) {
//             doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             doc.fontSize(10)
//             for (let j = 0; j < setOfChoice; j++) {
//                 doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).lineWidth(.1).stroke()
//                 doc.text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             }

//         }
//     }
// }
// else if (maxChoice == 20) {
//     gabForRowChoice = 14 * 9;
//     for (let r = 0; r < 4; r++) {
//         for (let j = 0; j < setOfChoice; j++) {
//             doc.fontSize(14).text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246, { width: 14, height: 14, align: 'center' })
//         }
//         for (let index = 1; index <= 5; index++) {
//             doc.fontSize(12).text(choiceNumber++, 79 + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             doc.fontSize(10)
//             for (let j = 0; j < setOfChoice; j++) {
//                 doc.circle(100 + (j * 14) + (r * gabForRowChoice), 253 + (index * 14), 5).lineWidth(.1).stroke()
//                 doc.text(choiceText[j], 93 + (j * 14) + (r * gabForRowChoice), 246 + (index * 14), { width: 14, height: 14, align: 'center' })
//             }

//         }
//     }
// }

// Finalize PDF file
QRCode.toDataURL('text', (err, url) => {
    if (err) throw err
    doc.image(url, 79, 40, {
        fit: [130, 89],
        align: 'center',
        valign: 'center'
    });
    doc.end();


});
var watchFile = fs.watch(fileName, () => {
    var pdfImage = new PDFImage(fileName);
    pdfImage.convertPage(0).then(function (imagePath) {
        if (fs.existsSync(imagePath)) watchFile.close();
    });
})
tmp.setGracefulCleanup();